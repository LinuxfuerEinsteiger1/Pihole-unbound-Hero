# **Pihole unbound Hero**


**Konzept: Auf einem RaspberryPI 4b 8GB ein Adblocker mit eigenem DNS Server zu installieren,
der wiederrum mit Tailscale (Mesh-Wireguard) von überall (z.B Handy) nutzbar ist.**



#**DynDNS auf dem Router!!!!**

ssh in den Raspi

#**Update/upgrade:**

> sudo apt update && sudo apt upgrade -y && sudo reboot now

ssh in den Raspi

#**Docker installieren:**

> curl -sSL https://get.docker.com | sh

#**Den Nutzer in die Docker Gruppe aufnehmen:**

> sudo usermod -aG docker $USER

> logout

ssh in den Raspi

> groups


#**Firewall Ports am Router freigeben:**

5335/tcp   /udp

53/tcp     /udp

443/tcp

#**Installation der Container starten:**

```
docker run -d --name pihole-unbound \
  --name=pihole-unbound \
  -e TZ=Europe/Berlin `#optional` \
  -p 53:53/tcp -p 53:53/udp \
  -p 80:80/tcp `#Pi-hole web port` \
  -e WEBPASSWORD='qwerty123' `#Buchstaben und Zahlen` \
  --restart=always \
  rlabinc/pihole-unbound:latest
```


> docker-compose down && docker-compose up -d

#**Um nun auf die Weboberfläche zu kommen:**

http://ip-des-raspi/admin





#**Notfall Tipps:**

#**Welche Container laufen:**

> docker ps -a

#**Stopen des Containers:**

docker container stop (Zahlen-und-Buchstaben-des-Containers)

#**Container löschen:**

docker container rm (Zahlen-und-Buchstaben-des-Containers)

#**Abhängigkeiten der defekten Container löschen um sauber neue Container auf zu setzen:**

> docker system prune -af &&     docker image prune -af &&     docker system prune -af --volumes &&     docker system df






#**Blocklisten:**

[Blockliste1](https://github.com/blocklistproject/Lists?tab=readme-ov-file#usage)

[Blockliste2](https://github.com/hagezi/dns-blocklists/blob/main/README.md)


#**Im Pihole unter der Rubrik :**
Settings -> DNS  folgenden DNS Server einzig eintragen:  127.0.0.1#5335
dort unten drunter : Permit all origins wählen

I****m Router verweist der erste DNS Eintrag auf die IPv4 des Raspi.

Der zweite IPv4 Eintrag empfiehlt sich ein alternativ DNS Server zu wählen. Bspw.
OpenDNS    208.67.222.222
           208.67.220.220


#**Tailscale:**

> curl -fsSL https://tailscale.com/install.sh | sh

> sudo tailscale up

> echo 'net.ipv4.ip_forward = 1' | sudo tee -a /etc/sysctl.d/99-tailscale.conf

> echo 'net.ipv6.conf.all.forwarding = 1' | sudo tee -a /etc/sysctl.d/99-tailscale.conf

> sudo sysctl -p /etc/sysctl.d/99-tailscale.conf

#**Bitte IP Adressbereiche anpassen:**

> sudo tailscale up --advertise-exit-node --accept-dns=false --accept-routes --advertise-routes=192.168.4.0/24,192.168.172.0/24


#**Automatische Updates setzen**

> sudo tailscale set --auto-update


#**Innerhalb der Tailscale Web Admin Konsole die Adressbereiche freigeben.**

#**Update/upgrade:**

> sudo apt update && sudo apt upgrade -y && sudo reboot now

ssh in den Raspi

#**Firewall UFW installieren:**

> sudo apt install ufw

> sudo ufw allow 5335/tcp

> sudo ufw allow 5335/udp

> sudo ufw allow 53/tcp

> sudo ufw allow 53/udp

> sudo ufw allow 80/tcp

> sudo ufw allow 443/tcp

#**Bitte den Adressbreich anpassen:**

> sudo ufw allow from 192.168.0.0/24 proto tcp to any port 22

> sudo ufw enable && sudo ufw reload

> sudo ufw status






#Quellen:

[text](https://pimylifeup.com/raspberry-pi-docker/)

[text](https://github.com/origamiofficial/docker-pihole-unbound)

[text](https://tailscale.com/kb/1103/exit-nodes)

[text](https://www.opendns.com/setupguide/)

[text](https://github.com/blocklistproject/Lists?tab=readme-ov-
file#usage)


[text](https://github.com/hagezi/dns-blocklists/blob/main/README.md)

[text](https://www.digitalocean.com/community/tutorials/ufw-
essentials-common-firewall-rules-and-commands)




**License**
The documentation in this project is licensed under the Creative Commons Attribution-ShareAlike 4.0 license, the source code content (also the source code included in the documentation) is licensed under the GNU GPLv3 license.




Author of Aknowledgement **T.Scheibelt**  felix.scheibelt@ik.me https://linuxfuereinsteiger.com

02/2024
